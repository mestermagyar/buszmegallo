#include <cstdio>
#include <vector>
#include <string>
#include <string.h>

using namespace std;

int checkCycle(const char (&s)[200],vector<vector<string>>& cycles);
int main()
{
    setlocale(LC_ALL,"hu_HU.utf8");
    FILE* T = fopen("lista","r");
    if(T==NULL)
    {
        printf("Error, file not found");
        return 0;
    }
    vector<vector<string>> cycles;
    char s[200];
    int checkVal;
    while(!feof(T))
    {
        if(fscanf(T,"%s",s) ==1)
        {
            checkVal=checkCycle(s,cycles);
            if(checkVal==-1)
            {
                cycles.push_back({s});

            }
            else
            {
                cycles[checkVal].push_back(s);
            }
        }

    }
    int mostVal=0;
    for(int i(0);i<cycles.size();i++)
    {
        if(cycles[i].size()>cycles[mostVal].size())mostVal=i;
    }
    for(string s:cycles[mostVal])
    {
        printf("%s\n", s.c_str());
    }
    fclose(T);
    return 0;
}

int checkCycle(const char (&s)[200],vector<vector<string>>& cycles)
{
    int SLength(strlen(s));
    for(int i(0); i<cycles.size(); i++)
    {
        if(cycles[i][0].size()==SLength)
        {
            vector<int> begVal;
            int f(0);
            while(f<SLength)
            {
                if(s[f]==cycles[i][0][0])
                {
                    begVal.push_back(f);
                }
                f++;
            }
            if(begVal.size()==0)
            {
                continue;
            }
            for(int e(0); e<cycles[i][0].size();f++,e++)
            {
                for(int g(0);g<begVal.size();g++)
                {
                    if(s[begVal[g]]==cycles[i][0][e])
                    {
                        (begVal[g]+1)==SLength ? begVal[g]=0 : ++begVal[g];
                    }
                    else
                    {
                        begVal.erase(begVal.begin()+(g-1));
                    }
                }
            }
            if(begVal.size()!=0)return i;
        }
    }
    return -1;
}
